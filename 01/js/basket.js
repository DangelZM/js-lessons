var basketModule = (function(){
    var basket = []; // private
    
    function addItem(values){
        basket.push(values);
        //console.log(basket);
        loger(basket);
    };
    
    function getItemCount(){
        return basket.length;
    };
    
    function getTotal(){
        var count = getItemCount(), total = 0;
        while(count--){
           total += basket[count].price;
        }
        return total;
    };
    
    function clear(){
        basket = [];
        //console.log(basket);
        loger(basket);
    };
    
    function loger(msg){
        console.log(msg);
    };
    
    // public
    return {
        addItem:      addItem,
        getTotal:     getTotal,
        getItemCount: getItemCount,
        clear: clear
    }
})();

var Mediator = (function(){
    var subscribe = function(){
            console.log('subscribe to event');
        },
        publish = function(){
            console.log('publish to event');
        };
    
    return {
        chanels: [],
        subscribe: subscribe,
        publish: publish,
        installTo: function(obj){
            obj.subscribe = this.subscribe;
            obj.publish = this.publish;
        }
    }
})();




