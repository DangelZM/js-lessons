define(['app/config'], function(config){
    var routes = config.routes;
    var defaultRoute = config.defaultRoute;
    var currentHash = '',
        refreshPage = false;

    function startRouting(){
        window.location.hash = window.location.hash || defaultRoute;
        setInterval(hashCheck, 100);
    }

    function getRoutes(){
        return routes;
    }

    function hashCheck(){
        if (window.location.hash != currentHash || refreshPage){
            if(refreshPage){
                refreshPage = false;
            };
            for (var i = 0, currentRoute; currentRoute = routes[i++];){
                if (window.location.hash == currentRoute.hash){
                    loadController(currentRoute.controller);
                }
            }
            currentHash = window.location.hash;
        }
    }
    
    function refresh(){
        refreshPage = true;
    };

    function loadController(controllerName){
        //console.log('loadController:' + controllerName);
        require(['controllers/' + controllerName], function(controller){
            controller.dispatch();
        });
    }

    return {
        refresh: refresh,
        getRoutes: getRoutes,
        startRouting:startRouting
    };
});