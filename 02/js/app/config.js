define(function(){
    return {
        $view: '#page-view',
        routes: [
            {hash:'#/home', controller:'dashboard'},
            {hash:'#/contact',  controller:'contact'}
        ],
        defaultRoute: '#/home'
    };
});