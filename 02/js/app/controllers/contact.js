define(['views/contact', 'app/components/toolbar'],
function(View, toolbar){
    'use strict';
    
    function dispatch(){
        toolbar.changeTitle('Contact');
        View.render();
    }; 
    
    return {
        dispatch: dispatch
    };
});