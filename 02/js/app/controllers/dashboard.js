define(['auth', 'app/config', 'views/dashboard', 'models/users', 'app/components/toolbar'],
function(auth, config, View, User, toolbar){
    'use strict';
    
    function dispatch(){
        toolbar.changeTitle('Dashboard');
        
        if(auth.getSession()){
            User.getFriends(function(data){
                View.render({users: data});
            });
        } else {
            $(config.$view).html('Not autorized user');
        };
    };
    
    return {
        dispatch: dispatch
    };
});