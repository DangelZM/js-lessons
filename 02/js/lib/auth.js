define(['vkontakte', 'app/router'], function(VK, router){
    'use strict';
    var session = null;
    
    function checkStatus(callback){
        VK.Auth.getLoginStatus(function(response) {
            if (response.session) {
                session = response.session;
            } else {
                session = null;
            };
            callback(session)
        });
    };
    
    function getSession(){
        return session;
    };
    
    function login(callback){
        VK.Auth.login(function(response){
            callback(response);
            session = response.session;
            router.refresh();
        }, 8192);
    };
    
    function logout(callback){
        VK.Auth.logout(function(response){
            callback(response);
            session = null;
            router.refresh();
        });
    };

    return {
        login: login,
        logout: logout,
        checkStatus: checkStatus,
        getSession: getSession
    };
});