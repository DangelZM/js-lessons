require.config({
    baseUrl: "js/lib",
    paths: {
        "app": "../app",
        "controllers": "../app/controllers",
        "models": "../app/models",
        "views": "../app/views",
        "jquery": "./jquery.min",
        "handlebars": "./handlebars.min",
        "templates": "../templates",
        //"facebook": '//connect.facebook.net/en_US/all',
        "vkontakte": '//vk.com/js/api/openapi'
    },
    shim: { 
        "handlebars": { exports: 'Handlebars' },
        //"facebook" : { exports: 'FB' },
        "vkontakte" : { exports: 'VK' }
    },
    urlArgs: "_=" + (new Date()).getTime()
});
//require(['fb']);
require(['vk']);