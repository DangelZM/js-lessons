# Примеры по JS Lessons  
##Полезные ссылки по метериалам:  
###Lessons 01  
[Бесплатная книга по паттернам в Javascript](http://addyosmani.com/resources/essentialjsdesignpatterns/book/)  
###Lessons 02  
[Scalable javascript application architecture (slides)](http://www.slideshare.net/nzakas/scalable-javascript-application-architecture)  
[AMD(Asynchronous Module Definition)](https://github.com/amdjs/amdjs-api/wiki/AMD)  
[RequireJS](http://requirejs.org/)  
#### Шаблонизаторы
[Handlebars](http://handlebarsjs.com/)  

[jQuery-Tmpl](https://github.com/BorisMoore/jquery-tmpl)  

[Mustache](https://github.com/janl/mustache.js)
